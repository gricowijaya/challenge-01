
-- library query 

-- one publisher can have many books

CREATE TABLE publishers(
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(50),
  address TEXT,
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW()
); 

-- insert publishers data

INSERT INTO publishers(name, address) 
VALUES ('PT media kara', 'Bandung, Jawa Barat' );

-- get the publishers

SELECT * FROM publishers;

-- one books can be had by many publisher

CREATE TABLE books(
  id BIGSERIAL PRIMARY KEY,
  publisher_id INTEGER,
  name VARCHAR(50),
  writer VARCHAR(50),
  release_date DATE,
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW(),
  FOREIGN KEY (publisher_id) REFERENCES publishers(id)
); 

-- insert the books by the name of the publishers

INSERT INTO books(publisher_id, name, writer, release_date) 
VALUES (1, 'aum cilengkram', 'dedens setyawan', '2022-08-02' );

-- get the publishers

SELECT 
publishers.name 
books.name,
books.writer
FROM books 
JOIN publishers ON publishere.id ;

-- users and profile (one on one)

CREATE TABLE users(
  id BIGSERIAL PRIMARY KEY,
  username VARCHAR(12) NOT NULL UNIQUE,
  password VARCHAR(12) NOT NULL,
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW()
); 

CREATE TABLE profiles(
  id BIGSERIAL PRIMARY KEY,
  fullname VARCHAR(50) NOT NULL,
  address TEXT,
  profile_id INTEGER,
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW(),
  FOREIGN KEY (profile_id) REFERENCES users(id)
);

-- insert the data for 
INSERT INTO users (username, password) VALUES ('gricowijaya', 'repassword');

-- Select through the data with no hashed password

SELECT username username_before_hashed, password password_before_hashed FROM users;

-- update psasword

UPDATE users SET password = ('cannotcrypt') WHERE username = 'gricowijaya';

SELECT username username_after_hashed, password password_after_hashed FROM users;

-- modify  
ALTER TABLE profiles ALTER column address TEXT;

INSERT INTO profiles (fullname, address, profile_id) VALUES ('Rico Wijaya', 'Jalan Mentari no 12, Denpasar, Bali', 1);

INSERT INTO profiles (fullname, address, profile_id) VALUES ('Rico Wijaya', 'Jalan Mentari no 12, Denpasar, Bali', 1);

-- SELECT the id before delete the duplicate data
SELECT id id_before_delete, fullname fullname_before_delete  FROM profiles;

DELETE FROM profiles where id  = 2;

SELECT id id_after_delete, fullname fullname_after_delete  FROM profiles;

-- create the book_stock

CREATE TABLE book_stocks (
  book_id INTEGER,
  stock INTEGER,
  FOREIGN KEY (book_id) REFERENCES books(id)
);

INSERT INTO book_stocks (book_id, stock) VALUES (1, 10);

-- we can get the name and the stock from here

SELECT books.name, book_stocks.stock FROM book_stocks 
JOIN books ON book_id = books.id;

-- create loan transaction

CREATE TABLE loan_transaction (
  id BIGSERIAL PRIMARY KEY,
  profile_id INTEGER,
  book_id INTEGER,
  amount_of_book INTEGER DEFAULT 1,
  loan_date DATE DEFAULT NOW() NOT NULL,
  return_date DATE,
  FOREIGN KEY(profile_id) REFERENCES profiles(id),
  FOREIGN KEY(book_id) REFERENCES books(id)
);

INSERT INTO loan_transaction(profile_id, book_id, amount_of_book)
VALUES (1, 1, 1);

SELECT p.fullname loaner, b.name book_that_loaned, lt.loan_date 
FROM loan_transaction lt 
JOIN profiles p  on lt.profile_id = p.id
JOIN books b on lt.book_id = b.id; 
