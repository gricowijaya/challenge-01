require('dotenv').config()

const { 
    HOST
} = process.env

console.log(HOST);

module.exports = {
    create: `${HOST}/user-game-history/create`,
    show: `${HOST}/user-game-history/get-history`,
}

