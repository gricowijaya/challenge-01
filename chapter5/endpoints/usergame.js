require('dotenv').config()

const { 
    HOST
} = process.env

module.exports = {
    index: `${HOST}/user-game/get-all`,
    update: `${HOST}/user-game/update-username`,
    show: `${HOST}/user-game/details`,
    destroy: `${HOST}/user-game/delete`
}

