require('dotenv').config()

const { 
    HOST
} = process.env
module.exports = {
    register: `${HOST}/auth/register`,
    login: `${HOST}/auth/login`,
    whoami: `${HOST}/auth/whoami`,
    changePassword: `${HOST}/auth/change-password`
}

