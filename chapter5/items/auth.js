const { Item } = require ('postman-collection')
const names = require('../names')
const endpoints = require('../endpoints')
const payloads = require('../payloads')
const headers = require('../headers')

// test if 200 then we can get the message of test successful kalau menggunakan jaz.
const requestTest = `
pm.test('Contoh Testing: Test for successfull response', function() {
    pm.expect(pm.response.code).to.equal(200);
});
`
const requests = { 
    register: new Item({
        name: names.auth.register,
        request: {
            header: headers.noAuth.requestHeader.register,
            url: endpoints.auth.register,
            method: 'POST',
            body: {
                mode: 'raw',
                raw: JSON.stringify(payloads.auth.register)
            },
            auth: null
        }, 
        events: [
            {
                listen: 'test',
                scripts: {
                    type: 'text/javascript',
                    exec: requestTest
                }
            }
        ]
    }),

    login: new Item({
        name: names.auth.login,
        request: {
            header: headers.noAuth.requestHeader.login,
            url: endpoints.auth.login,
            method: 'POST',
            body: {
                mode: 'raw',
                raw: JSON.stringify(payloads.auth.login)
            },
            auth: null
        }, 
        events: [
            {
                listen: 'test',
                scripts: {
                    type: 'text/javascript',
                    exec: requestTest
                }
            }
        ]
    }),

    whoami: new Item({
        name: names.auth.whoami,
        request: {
            header: headers.auth.requestHeader.whoami,
            url: endpoints.auth.whoami,
            method: 'GET',
            auth: null
        }, 
        events: [
            {
                listen: 'test',
                scripts: {
                    type: 'text/javascript',
                    exec: requestTest
                }
            }
        ]
    }),

    changePassword: new Item({
        name: names.auth.changePassword,
        request: {
            header: headers.auth.requestHeader.changePassword,
            url: endpoints.auth.changePassword,
            method: 'PUT',
            body: {
                mode: 'raw',
                raw: JSON.stringify(payloads.auth.changePassword)
            },
            auth: null
        }, 
        events: [
            {
                listen: 'test',
                scripts: {
                    type: 'text/javascript',
                    exec: requestTest
                }
            }
        ]
    }),
} 

module.exports = { requests }
