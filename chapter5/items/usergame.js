const { Item } = require ('postman-collection');
const names = require('../names');
const endpoints = require('../endpoints');
const payloads = require('../payloads');
const headers = require('../headers');

// test if 200 then we can get the message of test successful kalau menggunakan jaz.
const requestTest = `
pm.test('Contoh Testing: Test for successfull response', function() {
    pm.expect(pm.response.code).to.equal(200);
});
`
const requests = { 
    index : new Item({
        name: names.userGame.index,
        request: {
            header: headers.auth.requestHeader.indexUserGame,
            url: endpoints.userGame.index,
            method: 'GET',
            auth: null
        }, 
        events: [
            {
                listen: 'test',
                scripts: {
                    type: 'text/javascript',
                    exec: requestTest
                }
            }
        ]
    }),

    update: new Item({
        name: names.userGame.update,
        request: {
            header: headers.auth.requestHeader.updateUserGame,
            url: endpoints.userGame.update,
            method: 'PUT',
            body: {
                mode: 'raw',
                raw: JSON.stringify(payloads.userGame.update)
            },
            auth: null
        }, 
        events: [
            {
                listen: 'test',
                scripts: {
                    type: 'text/javascript',
                    exec: requestTest
                }
            }
        ]
    }),

    show: new Item({
        name: names.userGame.show,
        request: {
            header: headers.auth.requestHeader.showUserGame,
            url: endpoints.userGame.show,
            method: 'GET',
            auth: null
        }, 
        events: [
            {
                listen: 'test',
                scripts: {
                    type: 'text/javascript',
                    exec: requestTest
                }
            }
        ]
    }),

    destroy: new Item({
        name: names.userGame.destroy,
        request: {
            header: headers.auth.requestHeader.destroyUserGame,
            url: endpoints.userGame.destroy,
            method: 'DELETE',
            auth: null
        }, 
        events: [
            {
                listen: 'test',
                scripts: {
                    type: 'text/javascript',
                    exec: requestTest
                }
            }
        ]
    }),
} 

module.exports = { requests }
