require('dotenv').config()
const { Header } = require ('postman-collection')

const { 
    LOGIN_TOKEN
}  = process.env

const rawHeaderString =  
    `Authorization:${LOGIN_TOKEN}\nContent-Type:application/json\ncache-control:no-cache\n`;

// parse the headers into raw header string
const rawHeaders =  Header.parse(rawHeaderString);

// map the headers this headers should be formatted as an object keys because 
// the each request couldn't read the headers. So I specified the headers name 
// in here for each request in items/ 
const requestHeader = { 
    whoami: rawHeaders.map((h) => new Header(h)),
    changePassword: rawHeaders.map((h) => new Header(h)),
    indexUserGame: rawHeaders.map((h) => new Header(h)),
    updateUserGame: rawHeaders.map((h) => new Header(h)),
    showUserGame: rawHeaders.map((h) => new Header(h)),
    destroyUserGame: rawHeaders.map((h) => new Header(h)),
    createUserGameBiodata: rawHeaders.map((h) => new Header(h)),
    updateUserGameBiodata: rawHeaders.map((h) => new Header(h)),
    showUserGameBiodata: rawHeaders.map((h) => new Header(h)),
    destroyUserGameBiodata: rawHeaders.map((h) => new Header(h)),
    createUserGameHistory: rawHeaders.map((h) => new Header(h)),
    showUserGameHistory: rawHeaders.map((h) => new Header(h)),
}
module.exports = { requestHeader }

