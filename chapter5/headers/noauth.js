const { Header } = require ('postman-collection')

const rawHeaderString =  
    `Authorization:\nContent-Type:application/json\ncache-control:no-cache\n`;

// parse the headers into raw header string
const rawHeaders =  Header.parse(rawHeaderString);

// map the headers
const requestHeader =  {
    register: rawHeaders.map((h) => new Header(h)),
    login: rawHeaders.map((h) => new Header(h))
}

module.exports = { requestHeader }

