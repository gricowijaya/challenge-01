// userGame request_name
module.exports = { 
    index: `Get All User`,
    update: `Update User`,
    show: `Get Details User`,
    destroy: `Delete User`
}
