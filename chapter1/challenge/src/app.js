import volume from './volume.js'
import arithmetic from './arithmetic.js'
import area from './area.js'
import string from './string.js'
import readline from 'readline'


const app = {
  run: async() => {
    const readLine = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
      terminal: false
    });


    readLine.question(string.menu, input => {
      switch (input) {
        case '1':                                               
          volume.cube();                                        
          break;                                                
        case '2':                                               
          volume.cylinder();                                    
          break;                                                
        case '3':                                               
          arithmetic.addition();                                
          break;                                                
        case '4':                                               
          arithmetic.reduction();                               
          break;                                                
        case '5':                                               
          arithmetic.multiplication();                          
          break;                                                
        case '6':                                               
          arithmetic.division();                                
          break;                                                
        case '7':                                               
          arithmetic.power();                                   
          break;                                                
        case '8':
          arithmetic.root();
          break;
        case '9':
          area.square();
          break;
        default:
          console.log('Please Enter the correct number');
          app.run();
          break;
      }
    });

    // if the user is wanted to exit using Ctrl + C 
    // source : https://nodejs.org/api/readline.html#example-tiny-cli 
    // on section `Event: 'SIGINT'`
    readLine.on('SIGINT', () => {
      readLine.question('Are you sure you want to exit?\n>', (answer) => {
        if (answer.match(/^y(es)?$/i)) readLine.pause();
      });
    });
  }
} 

export default app;
