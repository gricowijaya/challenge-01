// This file is for calculating the an area.
import string from './string.js'
import readline from 'readline'

// PI constant
const PI = 3.14; 
const area = {
  // function for calculating circle area
  circle: (radius) => {
    try {
      return PI * radius**2;
    } catch(error) {
      throw error;
    }
  },

  square : () => {
    try {
      // create the interface for stdin and stdout
      const readLine = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
        terminal: false
      });

      // read the side value
      readLine.question(string.enterNumberSquare, side => {
        // count the cube
        console.log(`${string.resultSquare} ${side * side} m2`);
        // close the readLine
        readLine.close();
      });
    } catch(error) {
      throw error;
    }
  },
}

export default area;

