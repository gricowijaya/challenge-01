import readline from "readline"

const readLine = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

let array = []; 

const controller = {
  inputOutputNilai: async () => {
    try {

      const grade = await controller.checkGrade(array);
      readLine.question("> ", args => { 
          if (args == "q") { 
            array.sort((a, b) => a - b); // sort within the array by value


            console.log(`Nilai Tertinggi: ${ grade.nilaiTertinggi }`);
            console.log(`Nilai Terendah: ${ grade.nilaiTerendah }`);
            console.log(`Nilai Rata-rata: ${ grade.nilaiRataRata }`);
            console.log(`Nilai Passed: ${ grade.passed }`);
            console.log(`Nilai Not Passed: ${ grade.notPassed }`);
            console.log(`Nilai Terurut: ${ array }`);

            readLine.on('q', () => {
              readLine.question('Are you sure you want to exit?\n>', (answer) => {
                if (answer.match(/^y(es)?$/i)) readLine.pause();
              });
            });

            readLine.close();
          } else if (args > 100 || args < 0) {
            console.log("Input harus diantara 0 hingga 100");
            controller.inputOutputNilai();
          } else if (isNaN(args)){
            console.log("Input harus berupa angka");
            controller.inputOutputNilai();
          } else {
            array.push(parseInt(args));
            controller.inputOutputNilai()
          }
      });

    } catch (error) {
      throw error
    }
  },

  checkGrade: async (array) => {
    try {
      // if the grade is below 60 then it'll console.log(`Tidak Lulus`) 
      let grade = {
        passed: [],
        notPassed: [],
        nilaiTertinggi: null,
        nilaiRataRata: null,
        nilaiTerendah: null,
        nilai90dan100: []
      }


      array.forEach(element => {
        // after sorted
        grade.nilaiTertinggi = array[array.length - 1];
        grade.nilaiTerendah = array[0];

        // check if passed or not
        if (element >= 60) {
          grade.passed.push(element);
        } else {
          grade.notPassed .push(element);
        }

        // check the value if 90 or 100
        if (element == 90 || element == 100) { 
          grade.nilai90dan100.push(element);
        }
      });

      // get the average
      // and assign into grade.nilaiRataRata key
      const sum = array.reduce((total, current) => {
        return total + current;
      }, 0);
      const average = sum / array.length;
      
      grade.nilaiRataRata = average;

      return grade;
    } catch (error) {
      throw error;
    }
  }
}

export default controller;
